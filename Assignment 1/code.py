#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 12 11:59:38 2019

@author: perfectvips
"""
# Data-types
sample_int = 20
print("Type of value", sample_int, type(sample_int))

sample_float = 20.01
print("Type of value", sample_float, type(sample_float))

sample_complex = 20.01+3j
print("Type of value", sample_complex, type(sample_complex))

string = "My name is Diya"
string1 = 'I am working Perfectvips'
print(string)
print(string1)
print(string, "and", string1)
print(string+" and "+string1)

# Python Object
list = [1, 2, 3]
tuple = (1, 2, 3)

print(id(list))
print(id(tuple))

list += [4, 5]
tuple += (4, 5)

print(list)
print(tuple)

print(id(list))
print(id(tuple))

# Operators
# Python Arithmetic Operator
x = 15
y = 4
print('x + y =', x + y)
print('x - y =', x - y)
print('x * y =', x * y)
print('x / y =', x / y)
print('x // y =', x // y)
print('x ** y =', x ** y)
print('x % y =', x % y)

# Python Relational Operator
a = 10
b = 12
print('a > b  is', a > b)
print('a < b  is', a < b)
print('a == b is', a == b)
print('a != b is', a != b)
print('a >= b is', a >= b)
print('a <= b is', a <= b)

# Python Logical Operator
c = True
d = False
print('c and d is', c and d)
print('c or d is', c or d)
print('not c is', not c)

# Python Identity Operator
x1 = 5
y1 = 5
x2 = 'Hello'
y2 = 'Hello'

print(x1 is not y1)
print(x2 is y2)

# Python Bitwise Operator
e = 9
f = 65
print("Bitwise AND Operator On 9 and 65 is = ", e & f)
print("Bitwise OR Operator On 9 and 65 is = ", e | f)
print("Bitwise EXCLUSIVE OR Operator On 9 and 65 is = ", e ^ f)
print("Bitwise NOT Operator On 9 is = ", ~e)

print("Bitwise LEFT SHIFT Operator On 9 is = ", e << 1)
print("Bitwise RIGHT SHIFT Operator On 65 is = ", f >> 1)

# exc1
number = 1 + 2 * 3 / 4.0
print(number)

# exc2
remainder = 11 % 3
print(remainder)

# exc3
squared = 7 ** 2
cubed = 2 ** 3
print(squared)
print(cubed)

# exc4
helloworld = "hello" + " " + "world"
print(helloworld)

# exc5
lotsofhellos = "hello" * 10
print(lotsofhellos)

# exc6
a = 60            # 60 = 0011 1100
b = 13            # 13 = 0000 1101
c = 0

c = a & b        # 12 = 0000 1100
print("Line 1 - Value of c is ", c)

c = a | b        # 61 = 0011 1101
print("Line 2 - Value of c is ", c)

c = a ^ b       # 49 = 0011 0001
print("Line 3 - Value of c is ", c)

c = ~a          # -61 = 1100 0011
print("Line 4 - Value of c is ", c)

c = a << 2      # 240 = 1111 0000
print("Line 5 - Value of c is ", c)

c = a >> 2       # 15 = 0000 1111
print("Line 6 - Value of c is ", c)
