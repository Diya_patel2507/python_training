#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 13 15:33:54 2019

@author: perfectvips
"""

# Write a Python program to calculate the length of a string
string = "Hello, I am Diya"
print(len(string))

""" Write a Python program to get a single string from two given strings,
separated by a space."""
# Sample String : 'abc', 'xyz'
# Expected Result : 'abc xyz'
sam_string = 'abc'
sam_string1 = 'xyz'
print(sam_string+" "+sam_string1)

"""Write a Python program to add 'ing' at the end of a given string
 (length should be at least 3).
If the given string already ends with 'ing' then add 'ly' instead.I
If the string length of the given string is less than 3,leave it unchanged.
 Sample String : 'abc'
 Expected Result : 'abcing'
Sample String : 'string'
 Expected Result : 'stringly'"""
user = input("Enter a string : ")
if (len(user) >= 3):
    print("add ing:"+user+"ing")
    if(user.endswith('ing')):
        print("add ly:"+user+"ly")
else:
    print("Error :Length at least 3")

"""Write a Python program to find the first appearance of the substring 'not'
and 'poor' from a given string,
if 'not' follows the 'poor', replace the whole 'not'...'poor'
substring with 'good'.Return the resulting string.
Sample String : 'The lyrics is not that poor!'
'The lyrics is poor!'
Expected Result : 'The lyrics is good!'
'The lyrics is poor!'"""

user1 = input("Enter a string: ")
if("not" in user1 and "poor" in user1):
    print(user1.replace(user1[user1.find("not"):], "good"))
""" Write a Python program to remove the nth index character
 from a nonempty string.
Sample:
Enter string: "Hello Team"
Enter index to remove: 3
Expected Output:
"Helo Team" """

user2 = input("Enter a string: ")
new_string = user2[:3] + user2[4:]
print(new_string)

"""Write a program that will ask number from user and
check if number is even or odd."""

user3 = input("Enter a number: ")

if(int(user3) % 2 != 1):
    print("Number is even")
else:
    print("Number is odd")


"""Write a Python program that will ask number to user and check if
the number is divisible by 7 and multiple of 5?"""

user4 = input("Enter a number: ")

if(int(user4) % 7 == 0 and int(user4) % 5 == 0):
    print("True")


"""Write a Python program to convert temperatures to and
from celsius, fahrenheit.
   [ Formula : c/5 = f-32/9 [ where c = temperature in celsius
   and f = temperature in fahrenheit ]
   Expected Output :
   Enter temperatue in (°C):60
   60°C is 140 in Fahrenheit """
user5 = input("Enter temperatue in (°C):")
f = (9 * int(user5) / 5) + 32
print(user5 + "°C is " + str(f) + " in Fahrenheit")

"""Write a Python program to sum of two given integers.
However, if the sum is between 15 to 20 it will return 20."""

user6 = input("Enter Number1: ")
user7 = input("Enter Number2: ")
sum = int(user6) + int(user7)
if(sum >= 15 and sum <= 20):
    print("ans is 20")
else:
    print("ans is: "+str(sum))

"""Write a program in Python to calculate and
print the Electricity bill of a given customer.
The customer id., name and unit consumed by the user should be taken
from the keyboard and display the total amount to pay
to the customer. The charge are as follow :
Unit 					Charge/unit
upto 199 				@1.20
200 and above but less than 400 	@1.50
400 and above but less than 600 	@1.80
600 and above 				@2.00
If bill exceeds Rs. 400 then a surcharge of 15% will be charged
and the minimum bill should be of Rs. 100/-
"""

cid = input("Enter Customerid ")
name = input("Enter Name ")
unit = input("Enter unit ")
unit = int(unit)
if(unit <= 199):
    charge = 1.20
elif(unit >= 200 and unit < 400):
    charge = 1.50
elif (unit >= 400 and unit < 600):
    charge = 1.80
else:
    charge = 2.00
total_bill = float(unit) * charge

if(total_bill >= 100):
    if(total_bill >= 400):
        total = (total_bill * 15) / 100
        total_bill = total_bill + total
        print("Customer id: "+cid + " Name: "+name +
              " total amount to pay " + str(total_bill))
    else:
        print("Customer id: "+cid + " Name: "+name +
              " total amount to pay " + str(total_bill))
else:
    print("Min bill pay Rs.100")
